#![feature(is_sorted)]

use rand::Rng;

trait Shufflable<T> {
    fn shuffle(&mut self) -> T;
}

impl Shufflable<Vec<u8>> for Vec<u8> {
    fn shuffle(&mut self) -> Vec<u8> {
        let mut shuffled: Vec<u8> = Vec::new();
        let mut rng = rand::thread_rng();
        while self.len() > 0 {
            let index = rng.gen_range(0..self.len());
            let value = self.swap_remove(index);
            shuffled.push(value);
        }
        return shuffled;
    }
}


fn create_list(size: u8) -> Vec<u8> {
    let mut rng = rand::thread_rng();
    let mut list: Vec<u8> = Vec::new();
    for _ in 0..size {
        list.push(rng.gen_range(0..100));
    }
    return list;
}

fn main() {
    let mut list = create_list(5);
    let mut iterator: u64 = 0;
    println!("Random list is: {:?}", list);
    while !list.is_sorted() {
        iterator += 1;
        list = list.shuffle();
    }

    println!("Sorted list is: {:?}", list);
    println!("Tries: {:?}", iterator);
}
